package goostree.com.androidsqlite.data;

import android.provider.BaseColumns;

/**
 * Created by Darren on 2/16/2018.
 */

public final class SQLiteContract {
    //no accidental instantiation
    private SQLiteContract() {}

    //Define table contents
    public static class SQLiteEntry implements BaseColumns {
        public static final String DATABASE_NAME = "SQLite.db";
        public static final int DATABASE_VERSION = 1;
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_EMAIL = "email";
    }

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + SQLiteEntry.TABLE_NAME + " ( " +
                    SQLiteEntry._ID + " INTEGER PRIMARY KEY, " +
                    SQLiteEntry.COLUMN_NAME_NAME + " TEXT, " +
                    SQLiteEntry.COLUMN_NAME_EMAIL + " TEXT )";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + SQLiteEntry.TABLE_NAME;
}
