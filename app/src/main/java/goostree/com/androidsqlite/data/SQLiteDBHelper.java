package goostree.com.androidsqlite.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darren on 2/16/2018.
 */
public class SQLiteDBHelper extends SQLiteOpenHelper {

    public SQLiteDBHelper(Context context) {
        super(context, SQLiteContract.SQLiteEntry.DATABASE_NAME, null,
                SQLiteContract.SQLiteEntry.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQLiteContract.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(SQLiteContract.SQL_DROP_TABLE);
    }

    public List<String> getDbContents(){
        SQLiteDatabase db = getReadableDatabase();

        //Which columns
        String[] projection = {
                SQLiteContract.SQLiteEntry.COLUMN_NAME_NAME,
                SQLiteContract.SQLiteEntry.COLUMN_NAME_EMAIL
        };

        Cursor cursor = db.query(SQLiteContract.SQLiteEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        return cursorToList(cursor);
    }


    public void insertRow(String name, String email) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SQLiteContract.SQLiteEntry.COLUMN_NAME_NAME, name);
        values.put(SQLiteContract.SQLiteEntry.COLUMN_NAME_EMAIL, email);

        db.insert(SQLiteContract.SQLiteEntry.TABLE_NAME, null, values);
    }

    public void deleteRows() {
        SQLiteDatabase db = getWritableDatabase();

        //null for where clause deletes all
        db.delete(SQLiteContract.SQLiteEntry.TABLE_NAME, null, null);
    }

    private List<String> cursorToList(Cursor cursor){
        List<String> list =  new ArrayList<String>();

        while(cursor.moveToNext()) {
            list.add( cursor.getString(0) + " " + cursor.getString(1) );
        }

        return list;
    }

}

