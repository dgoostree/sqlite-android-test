package goostree.com.androidsqlite;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import goostree.com.androidsqlite.data.SQLiteDBHelper;

public class MainActivity extends AppCompatActivity {

    SQLiteDBHelper dbHelper = new SQLiteDBHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        populateTextView();
    }

    public void add(View view) {
        EditText nameView = (EditText)findViewById(R.id.nameText);
        String name = nameView.getText().toString();

        EditText emailView = (EditText)findViewById(R.id.emailText);
        String email = emailView.getText().toString();

        dbHelper.insertRow(name, email);

        populateTextView();
        clearInputs();
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }

    public void wipe(View view) {
        dbHelper.deleteRows();
        populateTextView();
        clearInputs();
    }

    private void populateTextView() {
        List<String> contents = dbHelper.getDbContents();

        StringBuilder builder = new StringBuilder();

        for(String str : contents) {
            builder.append(str);
            builder.append(System.getProperty("line.separator"));
        }

        TextView dbContentsText = (TextView) findViewById(R.id.textView);
        dbContentsText.setText(builder.toString());
    }

    private void clearInputs() {
        EditText nameView = (EditText)findViewById(R.id.nameText);
        nameView.setText("");
        EditText emailView = (EditText)findViewById(R.id.emailText);
        emailView.setText("");
    }
}
